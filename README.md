# oms
Critical edition of old Hungarian Planctus Mariae (13th c.) encoded in TEI XML.
Layers:
diplomatic and critical edition of source text (Codex Leuven, 134v.) with critical apparatus,
critical text with modernized spelling,
critical edition of short Latin planctus from B. Bischoff's edition.

Under CC-BY-NC-SA
